Rails.application.routes.draw do
  devise_for :users
  resources :users do
    resources :my_recipes, only: [:index, :create, :new, :edit, :update, :destroy]
    resources :my_fermentables, only: [:index, :new, :create, :destroy, :update, :edit]
    resources :my_hops, only: [:index, :new, :create, :destroy, :update, :edit]
    resources :my_yeasts, only: [:index, :new, :create, :destroy, :update, :edit]
  end
  resources :recipes, only: [:index, :show]
  resource :search, only: [:show]
  resources :fermentables_measurements, only: [:create]
  resources :hops_measurements, only: [:create]
  resources :yeasts_measurements, only: [:create]
  resources :fermentables
  resources :hops
  resources :yeasts
  root to: 'recipes#index'
end
