class Hop < ApplicationRecord
  has_many :hops_measurements
  has_many :recipes, through: :hops_measurements
  belongs_to :user
end
