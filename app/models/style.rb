class Style < ApplicationRecord
  has_many :recipes
end

class NullStyle
  def name
    ''
  end

  def style_family
    ''
  end

  def style_origin
    ''
  end

  def marked_for_destruction?
    false
  end
end
