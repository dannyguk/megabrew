class Recipe < ApplicationRecord
  searchkick
  has_many :fermentables_measurements, dependent: :destroy
  has_many :hops_measurements, dependent: :destroy
  has_many :yeasts_measurements, dependent: :destroy
  has_many :fermentables, through: :fermentables_measurements
  has_many :hops, through: :hops_measurements
  has_many :yeasts, through: :yeasts_measurements
  validates :name, presence: true
  belongs_to :user
  belongs_to :style

  def search_data
    {
      name: name,
      description: description,
      fermentables: fermentables.map(&:name),
      hops: hops.map(&:name),
      yeasts: yeasts.map(&:name),
      style_name: style_name
    }
  end

  def user_email
    user.email
  end

  def style
    super || NullStyle.new
  end

  def style_name
    style.name
  end
end
