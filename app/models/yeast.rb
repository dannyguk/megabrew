class Yeast < ApplicationRecord
  has_many :yeasts_measurements
  has_many :recipes, through: :yeasts_measurements
  belongs_to :user
end
