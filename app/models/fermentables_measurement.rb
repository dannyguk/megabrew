class FermentablesMeasurement < ApplicationRecord
  belongs_to :fermentable
  belongs_to :recipe
  belongs_to :user
end
