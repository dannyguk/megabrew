class Fermentable < ApplicationRecord
  has_many :fermentables_measurements
  has_many :recipes, through: :fermentables_measurements
  belongs_to :user
end
