class YeastsMeasurement < ApplicationRecord
  belongs_to :yeast
  belongs_to :recipe
  belongs_to :user
end
