class HopsMeasurement < ApplicationRecord
  belongs_to :hop
  belongs_to :recipe
  belongs_to :user
end
