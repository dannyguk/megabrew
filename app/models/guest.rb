class Guest
  def email
    'guest@megabrew.com'
  end

  def account_type
    class_name.parameterize
  end

  private

  def class_name
    self.class.name
  end
end
