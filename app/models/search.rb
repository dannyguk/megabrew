class Search
  attr_reader :query

  def initialize(query)
    @query = query
  end

  def recipes
    data.results
  end

  private

  def data
    @data ||= Recipe.search(query, aggs: aggs)
  end

  def aggs
    [:fermentables, :style_name]
  end
end
