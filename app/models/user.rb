class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :recipes
  has_many :fermentables
  has_many :hops
  has_many :yeasts
  has_many :fermentables_measurements
  has_many :hops_measurements
  has_many :yeasts_measurements

  def account_type
    class_name.parameterize
  end

  private

  def class_name
    self.class.name
  end
end
