json.extract! fermentable, :id, :created_at, :updated_at
json.url fermentable_url(fermentable, format: :json)