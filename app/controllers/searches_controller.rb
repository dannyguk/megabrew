class SearchesController < ApplicationController
  skip_filter :authenticate_user!, only: [:show]
  before_action :set_search, only: [:show]

  def show
    respond_to do |format|
      format.html {}
    end
  end

  private

  def set_search
    @search = Search.new(search_params[:q])
  end

  def search_params
    params.permit(:q)
  end
end
