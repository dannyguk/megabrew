class HopsMeasurementsController < ApplicationController
  def create
    @hop_measurement = current_user.hops_measurements.new(hops_measurement_params)

    respond_to do |format|
      if @hop_measurement.save
        format.html { redirect_to edit_user_my_recipe_path(current_user, @hop_measurement.recipe), notice: 'Hop was successfully added to recipe.' }
      else
        format.html { render @hop_measurement.recipe }
      end
    end
  end

  private

  def hops_measurement_params
    params.require(:hops_measurement).permit(:recipe_id, :hop_id)
  end
end
