class YeastsMeasurementsController < ApplicationController
  def create
    @yeast_measurement = current_user.yeasts_measurements.new(yeasts_measurement_params)

    respond_to do |format|
      if @yeast_measurement.save
        format.html { redirect_to edit_user_my_recipe_path(current_user, @yeast_measurement.recipe), notice: 'Yeast was successfully added to recipe.' }
      else
        format.html { render @yeast_measurement.recipe }
      end
    end
  end

  private

  def yeasts_measurement_params
    params.require(:yeasts_measurement).permit(:recipe_id, :yeast_id)
  end
end
