class FermentablesMeasurementsController < ApplicationController
  def create
    @fermentable_measurement = current_user.fermentables_measurements.new(fermentables_measurement_params)

    respond_to do |format|
      if @fermentable_measurement.save
        format.html { redirect_to edit_user_my_recipe_path(current_user, @fermentable_measurement.recipe), notice: 'Fermentable was successfully added to recipe.' }
      else
        format.html { render @fermentable_measurement.recipe }
      end
    end
  end

  private

  def fermentables_measurement_params
    params.require(:fermentables_measurement).permit(:recipe_id, :fermentable_id)
  end
end
