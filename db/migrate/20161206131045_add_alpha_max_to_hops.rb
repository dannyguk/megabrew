class AddAlphaMaxToHops < ActiveRecord::Migration[5.0]
  def change
    add_column :hops, :alpha_max, :float
  end
end
