class CreateStyles < ActiveRecord::Migration[5.0]
  def change
    create_table :styles do |t|
      t.string :category_id
      t.string :bjcp_category
      t.string :name
      t.string :style_family
      t.text :style_history
      t.string :style_origin
      t.text :overall_impression
      t.text :aroma
      t.text :appearance
      t.text :flavour
      t.text :mouthfeel
      t.text :comments
      t.text :history
      t.text :ingredients
      t.text :comparison
      t.text :examples
      t.decimal :abv_min
      t.decimal :abv_max
      t.decimal :ibus_min
      t.decimal :ibus_max
      t.decimal :srm_min
      t.decimal :srm_max
      t.decimal :og_min
      t.decimal :og_max
      t.decimal :fg_min
      t.decimal :fg_max

      t.timestamps
    end
  end
end
