class AddUserIdToHops < ActiveRecord::Migration[5.0]
  def change
    add_column :hops, :user_id, :integer
  end
end
