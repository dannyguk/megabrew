class AddFieldsToYeasts < ActiveRecord::Migration[5.0]
  def change
    add_column :yeasts, :attenuation_min, :integer
    add_column :yeasts, :attenuation_max, :integer
    add_column :yeasts, :temperature_min, :integer
    add_column :yeasts, :temperature_max, :integer
  end
end
