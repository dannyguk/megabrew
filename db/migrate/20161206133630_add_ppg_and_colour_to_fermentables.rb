class AddPpgAndColourToFermentables < ActiveRecord::Migration[5.0]
  def change
    add_column :fermentables, :ppg, :integer
    add_column :fermentables, :colour, :integer
  end
end
