class CreateHopsMeasurements < ActiveRecord::Migration[5.0]
  def change
    create_table :hops_measurements do |t|
      t.integer :recipe_id
      t.integer :user_id
      t.integer :hop_id

      t.timestamps
    end
  end
end
