class CreateRecipes < ActiveRecord::Migration[5.0]
  def change
    create_table :recipes do |t|
      t.text :name
      t.text :description
      t.integer :user_id, null: false
      t.timestamps
    end
  end
end
