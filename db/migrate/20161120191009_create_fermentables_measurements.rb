class CreateFermentablesMeasurements < ActiveRecord::Migration[5.0]
  def change
    create_table :fermentables_measurements do |t|
      t.integer :recipe_id
      t.integer :user_id
      t.integer :fermentable_id

      t.timestamps
    end
  end
end
