class RenameAlphaToAlphaMinOnHops < ActiveRecord::Migration[5.0]
  def change
    rename_column :hops, :alpha, :alpha_min
  end
end
