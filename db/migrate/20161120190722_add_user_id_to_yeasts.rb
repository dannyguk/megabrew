class AddUserIdToYeasts < ActiveRecord::Migration[5.0]
  def change
    add_column :yeasts, :user_id, :integer
  end
end
