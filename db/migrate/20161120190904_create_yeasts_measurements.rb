class CreateYeastsMeasurements < ActiveRecord::Migration[5.0]
  def change
    create_table :yeasts_measurements do |t|
      t.integer :recipe_id
      t.integer :user_id
      t.integer :yeast_id

      t.timestamps
    end
  end
end
