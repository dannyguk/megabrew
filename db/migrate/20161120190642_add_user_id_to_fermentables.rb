class AddUserIdToFermentables < ActiveRecord::Migration[5.0]
  def change
    add_column :fermentables, :user_id, :integer
  end
end
