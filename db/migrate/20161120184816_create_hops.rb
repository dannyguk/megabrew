class CreateHops < ActiveRecord::Migration[5.0]
  def change
    create_table :hops do |t|
      t.string :name
      t.text :description
      t.float :alpha

      t.timestamps
    end
  end
end
