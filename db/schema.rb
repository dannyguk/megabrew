# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161206133630) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "fermentables", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.float    "ebc"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
    t.integer  "ppg"
    t.integer  "colour"
  end

  create_table "fermentables_measurements", force: :cascade do |t|
    t.integer  "recipe_id"
    t.integer  "user_id"
    t.integer  "fermentable_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "hops", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.float    "alpha_min"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
    t.float    "alpha_max"
  end

  create_table "hops_measurements", force: :cascade do |t|
    t.integer  "recipe_id"
    t.integer  "user_id"
    t.integer  "hop_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recipes", force: :cascade do |t|
    t.text     "name"
    t.text     "description"
    t.integer  "user_id",     null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "style_id"
  end

  create_table "styles", force: :cascade do |t|
    t.string   "category_id"
    t.string   "bjcp_category"
    t.string   "name"
    t.string   "style_family"
    t.text     "style_history"
    t.string   "style_origin"
    t.text     "overall_impression"
    t.text     "aroma"
    t.text     "appearance"
    t.text     "flavour"
    t.text     "mouthfeel"
    t.text     "comments"
    t.text     "history"
    t.text     "ingredients"
    t.text     "comparison"
    t.text     "examples"
    t.decimal  "abv_min"
    t.decimal  "abv_max"
    t.decimal  "ibus_min"
    t.decimal  "ibus_max"
    t.decimal  "srm_min"
    t.decimal  "srm_max"
    t.decimal  "og_min"
    t.decimal  "og_max"
    t.decimal  "fg_min"
    t.decimal  "fg_max"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "yeasts", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "user_id"
    t.integer  "attenuation_min"
    t.integer  "attenuation_max"
    t.integer  "temperature_min"
    t.integer  "temperature_max"
  end

  create_table "yeasts_measurements", force: :cascade do |t|
    t.integer  "recipe_id"
    t.integer  "user_id"
    t.integer  "yeast_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
