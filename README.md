# MegaBrew

## Contributing
Make any contributions on a separate branch and create a PR into master (will create a staging branch later when needed).

## Development environment

### Ruby (2.3.1)
Ruby-2.3.1 (best installed via RVM: https://rvm.io/)
rvm install ruby-2.3.1
check ruby version `ruby -v`
if not 2.3.1 `rvm use ruby-2.3.1`

### Postgres (9.6)
http://postgresapp.com/
Download, copy to applications and open it.

### Instructions
Clone repository
* run `gem install bundler`
* run `bundle`
* run `rails db:create`
* run `rails db:migrate`
* run `rails s` to start server: http://localhost:3000
* run `rails c` to access development console
