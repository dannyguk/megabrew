namespace :once do
  task :create_styles => :environment do
    require 'csv'
    CSV.foreach("#{Rails.root}/db/styles.csv", headers: true) do |row|
      style =
        Style.find_or_create_by(
          category_id:        row[0],
          bjcp_category:      row[1],
          name:               row[2],
          style_family:       row[3],
          style_history:      row[4],
          style_origin:       row[5],
          abv_min:            row[6].to_f,
          abv_max:            row[7].to_f,
          ibus_min:           row[8].to_f,
          ibus_max:           row[9].to_f,
          srm_min:            row[10].to_f,
          srm_max:            row[11].to_f,
          og_min:             row[12].to_f,
          og_max:             row[13].to_f,
          fg_min:             row[14].to_f,
          fg_max:             row[15].to_f,
          overall_impression: row[16],
          aroma:              row[17],
          appearance:         row[18],
          flavour:            row[19],
          mouthfeel:          row[20],
          comments:           row[21],
          history:            row[22],
          ingredients:        row[23],
          comparison:         row[24],
          examples:           row[25]
        )
      puts "Created #{style.category_id} - #{style.name}"
    end
  end

  task create_hops: :environment do
    file_path = File.join(Rails.root, 'db', 'hops.json')
    file = File.read(file_path)
    hops_hash = JSON.parse(file)
    hops_hash.each do |hop|
      hop =
        Hop.find_or_create_by(
          name: hop['name'],
          alpha_min: hop['alpha_min'],
          alpha_max: hop['alpha_max'],
          description: hop['notes']
        )
      puts "Created #{hop.id} - #{hop.name}"
    end
  end

  task create_yeasts: :environment do
    file_path = File.join(Rails.root, 'db', 'yeasts.json')
    file = File.read(file_path)
    yeasts_hash = JSON.parse(file)
    yeasts_hash.each do |yeast|
      yeast =
        Yeast.find_or_create_by(
          name: yeast['name'],
          description: yeast['notes'],
          attenuation_min: yeast['attenuation_min'],
          attenuation_max: yeast['attenuation_max'],
          temperature_min: yeast['temperature_min'],
          temperature_max: yeast['temperature_max']
        )
      puts "Created #{yeast.id} - #{yeast.name}"
    end
  end

  task create_fermentables: :environment do
    file_path = File.join(Rails.root, 'db', 'fermentables.json')
    file = File.read(file_path)
    fermentables_hash = JSON.parse(file)
    fermentables_hash.each do |fermentable|
      fermentable =
        Fermentable.find_or_create_by(
          name: fermentable['name'],
          description: fermentable['notes'],
          ppg: fermentable['ppg'],
          colour: fermentable['color']
        )
      puts "Created #{fermentable.id} - #{fermentable.name}"
    end
  end
end
